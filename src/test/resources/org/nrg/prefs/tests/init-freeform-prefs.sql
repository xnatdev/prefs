-- DROP TABLE IF EXISTS xhbm_preference;
-- DROP TABLE IF EXISTS xhbm_tool;

-- CREATE TABLE xhbm_tool (
--     id BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
--     created timestamp without time zone NOT NULL,
--     disabled timestamp without time zone NOT NULL,
--     enabled boolean DEFAULT true,
--     timestamp timestamp without time zone NOT NULL,
--     resolver character varying(255),
--     strict boolean NOT NULL,
--     tool_description character varying(255),
--     tool_id character varying(255) NOT NULL,
--     tool_name character varying(255) NOT NULL
-- );

-- CREATE TABLE xhbm_preference (
--     id BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
--     created timestamp without time zone NOT NULL,
--     disabled timestamp without time zone NOT NULL,
--     enabled boolean DEFAULT true,
--     timestamp timestamp without time zone NOT NULL,
--     entity_id character varying(255),
--     name character varying(255) NOT NULL,
--     scope integer,
--     value character varying(65535) NOT NULL,
--     tool bigint REFERENCES xhbm_tool (id),
--     UNIQUE (tool, name, scope, entity_id)
-- );

INSERT INTO xhbm_tool (id, created, disabled, timestamp, enabled, strict, tool_description, tool_id, tool_name) VALUES (1000, now(), TIMESTAMP '1969-12-31 18:00:00', now(), 't', 'f', 'Manages site configurations and settings for the XNAT system.', 'siteConfig', 'XNAT Site Preferences');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'adminEmail', 'xnatselenium@gmail.com');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'aliasTokenTimeout', '2 days');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'aliasTokenTimeoutSchedule', '0 0 * * * *');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'allowDataAdmins', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'allowHtmlResourceRendering', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'allowNonAdminsToClaimUnassignedSessions', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'archivePath', '/data/xnat/archive');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'buildPath', '/data/xnat/build');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'cachePath', '/data/xnat/cache');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'checksums', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'dataPaths', '["/xapi/**", "/data/**", "/REST/**", "/fs/**"]');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enableCsrfToken', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enableDicomReceiver', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enableDicomReceiverPropertyChangedListener', 'org.nrg.dcm.DicomSCPSiteConfigurationListener');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enableSitewideAnonymizationScript', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enableSitewideSeriesImportFilter', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'enabledProviders', '["localdb"]');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'ftpPath', '/data/xnat/ftp');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'inactivityBeforeLockout', '1 year');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'inactivityBeforeLockoutSchedule', '0 0 1 * * ?');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'inboxPath', '/data/xnat/inbox');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'initialized', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'interactiveAgentIds', '[".*MSIE.*", ".*Mozilla.*", ".*AppleWebKit.*", ".*Opera.*"]');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'maxFailedLogins', '20');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'maxFailedLoginsLockoutDuration', '1 hour');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordComplexity', '^.*$');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordComplexityMessage', 'Password is not sufficiently complex.');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordExpirationDate', '');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordExpirationInterval', '1 year');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordExpirationType', 'Interval');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordHistoryDuration', '1 year');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'passwordReuseRestriction', 'None');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'pipelinePath', '/data/xnat/pipeline');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'prearchivePath', '/data/xnat/prearchive');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'primaryAdminUsername', 'admin');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'processingUrl', '');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'requireChangeJustification', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'requireEventName', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'requireImageAssessorLabels', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'requireLogin', 'true');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'requireSaltedPasswords', 'false');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'sessionTimeout', '60 minutes');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'sessionTimeoutMessage', 'Session timed out at TIMEOUT_TIME.');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'siteId', 'XNAT');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'siteLogoPath', '/images/logo.png');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1969-12-31 18:00:00', now(), 't', 1000, 0, NULL, 'siteUrl', 'http://localhost:8080');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1970-01-01 00:00:00', now(), 't', 1000, 0, '\N', 'triagePath1', '');
INSERT INTO xhbm_preference (created, disabled, timestamp, enabled, tool, scope, entity_id, name, value) VALUES (now(), '1970-01-01 00:00:00', now(), 't', 1000, 0, NULL, 'triagePath2', '');
